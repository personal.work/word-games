########## PSUEDOCODE BEGIN ##########




# The goal: A game set up like Wordle, played in the terminal, which gives you one round
# per letter of the word to figure out the solution.

# I need a "for loop" with len to loop through the rounds, I need a list of words varying in
# length, and I need to match the indexed letters to the original word.

# for every letter in the correct word,
#     ask for the input of a new word
#     for every letter in the input word,
#         if letter is in the correct word, add a yellow square to list
#             if letter is the same as the letter at that index in the correct word, add letter to list
#         if letter is not in the word, add a red square to list
#     for every item in list:
#         print the item





########## PSEUDOCODE END ##########

import random

wordlist = [
    "hyena",
    "koala",
    "camel",
    "eagle",
    "goose",
    "llama",
    "mouse",
    "otter",
    "sheep",
    "snail",
    "snake",
    "squid",
    "skunk",
    "tiger",
    "zebra",
    "puppy",
    "panda",
    "kitty",
    "bunny",
    "shark",
]

correctword = random.choice(wordlist)
print("*")
print("*")
print("*")
print("*")
print("This word game is for guessing an animal's name.")
print("You get five chances, and will be told how many letters you have right every time!")
print("Yellow squares mean the letter is in there, but not in the right place.")
print("Red squares mean to try again!")
print("*")
print("*")
print("*")
print("*")
for round in range(len(correctword)):
    guessresults = []
    guessword = input("What is your five-letter guess? ")
    for index, letter in enumerate(guessword):
        if letter in correctword:
            if letter == correctword[index]:
                guessresults.append(letter.upper())
            else:
                guessresults.append("🟨")
        else:
            guessresults.append("🟥")
    for item in guessresults:
        print(item)
    if guessword == correctword:
        print("YOU WIN!!!")
        break
print(f"Your word was: {correctword.upper()}!")
